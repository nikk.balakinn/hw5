import zipfile

PASSWORD_LENGTH = 4


def extract_archive(file_to_open, password):
    try:
        file_to_open.extractall(pwd=password.encode())
        return True
    except Exception as e:
        print(e)
        return False

def hack_archive(task):
    file_to_open = zipfile.ZipFile(task)
    for i in range (10000):
        pwd = str(i).zfill(PASSWORD_LENGTH)
        print(pwd)
        if extract_archive(file_to_open, pwd):
            break
        print(extract_archive(file_to_open, pwd))
    print(f'Archive {task} is hacked. Password - {pwd}')
    print(f'Password was found after {i} tries')

filename = 'task.zip'
hack_archive(filename)
